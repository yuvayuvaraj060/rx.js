const { interval } = require("rxjs");
const { mapTo, scan, filter } = require("rxjs/operators");

const intervalSource = interval(1000);

intervalSource
  .pipe(
    mapTo(-1),
    scan((acc, currentValue) => acc + currentValue, 10),
    filter((value) => value >= 0)
  )
  .subscribe((value) => {
    console.log(value);
    if (!value) {
      console.log("count Down complected");
    }
  });
