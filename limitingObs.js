const { interval } = require("rxjs");
const { scan, mapTo, take, takeWhile } = require("rxjs/operators");

const countDown = interval(1000);

countDown
  .pipe(
    mapTo(-1),
    scan((acc, current) => {
      return acc + current;
    }, 3),
    takeWhile(value => value >= 0)
  )
  .subscribe((value) => {
    console.log(value);
  });


  