const { Observable, range } = require("rxjs");
const observer = {
  next: (value) => console.log(value),
  error: (value) => console.error(value),
  complete: () => console.log("Completed"),
};

const observable = new Observable((subscribe) => {
  subscribe.next("1st rx.js line code");
  subscribe.complete();
});
// observable.subscribe(observer);

