const { fromEvent, of, range, from, interval, timer } = require("rxjs");
const { map, filter, reduce, scan } = require("rxjs/operators");

const fetch = require("node-fetch");
const observer = {
  next: (value) => console.log(value),
  error: (error) => console.log(error),
  complete: () => console.log("done"),
};
// !from event creator

// const source = fromEvent(document, "click");

// source.subscribe(observer);

// !of and range operators

// !of
const ofOperator = of(1, 2, 3, 4, 5, 6, 7);
// ofOperator.subscribe(observer);

// !range operator

const rangeOperator = range(1, 7);
// rangeOperator.subscribe(observer);

// !from operators
const fromOperator = from(
  fetch("https://jsonplaceholder.typicode.com/todos/1")
);

// fromOperator.subscribe(observer);

// !from example and use case
function* generator() {
  yield "hai";
  yield "bye";
}

const ex1 = from(generator());
// ex1.subscribe(observer);

// !intervale

const intervalOperator = interval(500);
// intervalOperator.subscribe(console.log);

// !timer
const timerOperator = timer(4000, 500);
// timerOperator.subscribe(console.log);

//! pip with operators
// const rangeOperator1 = range(0, 6).pipe(map((num) => num + 10)).subscribe(console.log);

// filter operator with pipe
// range(0, 10)
//   .pipe(filter((num) => num > 3))
//   .subscribe(console.log);

//! reducer operator
// accumulator value is total value and the current value is currently accessed by index in the array

// range(0, 10)
//   .pipe(
//     reduce((accumulator, currentValue) => {
//       return accumulator + currentValue;
//     }, 0)
//   )
//   .subscribe(console.log);

//   scan
range(0, 10)
  .pipe(
    scan((accumulator, currentValue) => {
      return accumulator + currentValue;
    }, 0)
  )
  .subscribe(console.log);


